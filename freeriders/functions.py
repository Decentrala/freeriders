import time
import datetime

def datetounix(day, month, year, hour, minute, second):
    seconds = datetime.datetime(year, month, day, hour, minute, second).timestamp()
    return int(seconds)

def formatprefix2(number):
    return "{:02d}".format(number)

def formatprefix10(number):
    return "{:010d}".format(number)

def predict(timestamp):
        base_ticket = 7157662
        base_timestamp = 1695887564
        step = 0.688

        ticket = base_ticket + (timestamp - base_timestamp) * step
        return int(ticket)

