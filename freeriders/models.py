from freeriders import db

class Tickets(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    ticket = db.Column(db.Integer, nullable=False)
    timestamp = db.Column(db.Integer, nullable=False)
