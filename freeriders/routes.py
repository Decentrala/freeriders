from flask import render_template, request, redirect
from freeriders import app, db
from freeriders.functions import formatprefix2,formatprefix10, predict
from freeriders.models import Tickets
from datetime import datetime
import time

@app.route('/sms', methods=['GET'])
def sms():
    if request.method == 'GET':
        timenow = int(time.time())

        try:
            lastticket = Tickets.query.order_by(Tickets.timestamp.desc()).first()

            if lastticket.timestamp < timenow - (90 * 60):
                lastticket = formatprefix10(predict(timenow))
            else:
                lastticket = formatprefix10(lastticket.ticket)
            date = datetime.now()
            datenow = f'{formatprefix2(date.day)}.{formatprefix2(date.month)}.{date.year}'
            timenow = f'{formatprefix2(date.hour)}:{formatprefix2(date.minute)}:{formatprefix2(date.second)}'
            return render_template('sms.html', ticket = lastticket, date = datenow, time = timenow)
        except:
            return 'Error retriving last ticket'
    else:
        return 'HTTP request method not recogniezed'

@app.route('/submit', methods=['POST', 'GET'])
def submit():
    PREDICTTIMERANGE = 60 * 60 * 24
    if request.method == 'GET':
        return render_template('submit.html')
    elif request.method == 'POST':
        timenow = int(time.time())
        ticket_input = request.form['ticket']

        if ticket_input.isdigit() and len(ticket_input) == 10 :
            if int(ticket_input) < predict(timenow + PREDICTTIMERANGE ) and int(ticket_input) > predict(timenow - PREDICTTIMERANGE ) :
                ticket = Tickets(ticket = int(ticket_input), timestamp = timenow)
            else:
                print(int(ticket_input))
                print(predict(timenow - PREDICTTIMERANGE))
                print(predict(timenow + PREDICTTIMERANGE))
                print(predict(timenow))
                return 'Ticket number is in unexpected range.'
        else:
            return 'Ticket format is wrong. Only 10 digits allowed.'

        try:
            db.session.add(ticket)
            db.session.commit()
            return 'Ticket added'
        except:
            return 'Adding ticket failed'

    else:
        return 'HTTP request method not recogniezed'
