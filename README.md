# freeriders

Web app for tracking current ticket numbers

# Architecture

- keep all ticket numbers and timestamps in SQL table "tickets"
- display last ticket number
- don't allow tickets that are out of probable ticket number range
- if last submited ticket is timestamped more then 90 minutes ago, try to predict the next number
